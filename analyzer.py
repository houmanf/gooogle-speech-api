import json

SPLITTER = ' '

f = open('transcription.txt', 'r')

wordCount = 0

confidenceArray = []
for line in f:
	partitionedLine = line.partition(':')
	lineId = partitionedLine[0]
	jsonString = partitionedLine[2]
	jsonData = json.loads(jsonString)

	status = jsonData['status']
	hypotheses = jsonData['hypotheses']
	if status == 0:
		confidence = hypotheses[0]['confidence']
		utterance = hypotheses[0]['utterance']
		confidenceArray.append(confidence)
		print lineId + " " + utterance + " " + str(round(confidence,2))
		wordCount += len(utterance.split(SPLITTER))
		
# calculations

averageConfidence = 0
if float(len(confidenceArray)) != 0:
	averageConfidence = sum(confidenceArray) / float(len(confidenceArray))

statistics = '\n'
statistics += 'Confidence Average = %f\n' % round(averageConfidence, 2)
statistics += 'Word Count = %i\n' % (wordCount)

print statistics
f.close()
