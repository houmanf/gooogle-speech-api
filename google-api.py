import urllib2, json

RETRY_COUNT = 3

url = "https://www.google.com/speech-api/v1/recognize?xjerr=1&client=chromium&lang=en-US"
f = open('', 'w')

i = 1;
totalTimeoutCount = 0
lossTranscriptionCount = 0
retryCount = RETRY_COUNT
while i in range (1, 600):
	path = "%i.flac" % (i)
	print path
	audio = open(path,'rb').read()
	headers={'Content-Type': 'audio/x-flac; rate=8000', 'User-Agent':'speech2text'}
	try:
		request = urllib2.Request(url, data=audio, headers=headers)
		response = urllib2.urlopen(request)
		jsonResponse = response.read();
		
		messageLines = jsonResponse.split('\n')
		for line in messageLines:
			if line != "" and '"status":5' not in line:
				message = "%i: %s\n" % (i, line) 
				f.write(message)
				print jsonResponse
		i = i + 1;
				
	except urllib2.HTTPError:
		totalTimeoutCount = totalTimeoutCount + 1
		retryCount = retryCount - 1
		if retryCount == 0:
			retryCount = RETRY_COUNT
			i = i + 1
			lossTranscriptionCount = lossTranscriptionCount + 1
		pass

statistics = '\n'
statistics += 'TOTAL TIMEOUTS = %i\n' % (totalTimeoutCount)
statistics += 'LOSS TRANSCRIPTIONS = %i\n' % (lossTranscriptionCount)

print statistics

f.close();
